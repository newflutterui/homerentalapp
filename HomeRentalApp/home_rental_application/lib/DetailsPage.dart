import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  const Detail({super.key});

  @override
  State<Detail> createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            const SizedBox(
              height: 40,
            ),
            Row(
              children: [
                const Padding(
                  padding: EdgeInsets.all(10),
                ),
                Container(
                  height: 30,
                  width: 30,
                  decoration: const BoxDecoration(
                    //color: Colors.amber,
                    image: DecorationImage(
                      image: AssetImage('assets/images/image10.png'),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 150,
                ),
                const Text(
                  'Details',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 244,
              width: 450,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/images/image11.png'),
                ),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // const SizedBox(
                  //   width: 124,
                  // ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Container(
                      height: 27.04,
                      width: 55.95,
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(112, 200, 250, 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(4),
                          ),
                          Container(
                            height: 19.82,
                            width: 19.82,
                            child: Image.asset('assets/images/image12.png'),
                          ),
                          const SizedBox(
                            width: 3,
                          ),
                          Container(
                            height: 24,
                            width: 24,
                            child: const Text(
                              '4.9',
                              style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20),
                ),
                Text(
                  'Night Hill Villa',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 230,
                        ),
                        Text(
                          '4500',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            color: Color.fromRGBO(32, 169, 247, 1),
                          ),
                        ),
                        Text(
                          '/Month',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20),
                ),
                Text(
                  'London,Night Hill',
                  style: TextStyle(
                    color: Color.fromRGBO(72, 72, 72, 1),
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20),
                ),
                Container(
                  height: 141,
                  width: 112,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          const Padding(padding: EdgeInsets.only(left: 15)),
                          Container(
                            height: 30,
                            width: 30,
                            child: Image.asset('assets/images/image4.png'),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 15)),
                          Text(
                            'Bedrooms',
                            style: TextStyle(
                              color: Color.fromRGBO(72, 72, 72, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 15)),
                          Text(
                            '5',
                            style: TextStyle(
                              //color: Color.fromRGBO(72, 72, 72, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Container(
                  height: 141,
                  width: 112,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          const Padding(padding: EdgeInsets.only(left: 15)),
                          Container(
                            height: 30,
                            width: 30,
                            child: Image.asset('assets/images/image5.png'),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 15)),
                          Text(
                            'Bathrooms',
                            style: TextStyle(
                              color: Color.fromRGBO(72, 72, 72, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 15)),
                          Text(
                            '6',
                            style: TextStyle(
                              //color: Color.fromRGBO(72, 72, 72, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Container(
                  height: 141,
                  width: 112,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          const Padding(padding: EdgeInsets.only(left: 15)),
                          Container(
                            height: 30,
                            width: 30,
                            child: Image.asset('assets/images/image6.png'),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 15)),
                          Text(
                            'Square ft',
                            style: TextStyle(
                              color: Color.fromRGBO(72, 72, 72, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 15)),
                          Text(
                            '7,000 sq ft',
                            style: TextStyle(
                              //color: Color.fromRGBO(72, 72, 72, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            const Row(
              children: [
                Padding(padding: EdgeInsets.all(10)),
                Text(
                  'Amet minim mollit non deserunt ullamco est \nsit aliqua dolor do amet sint. Velit officia \nconsequat duis enim velit mollit. Exercitation \nveniam consequat sunt nostrud amet. Amet \nminim mollit non deserunt ullamco est sit \naliqua dolor do amet sint. Velit officia consequat \nduis enim velit mollit. Exercitation veniam \nconsequat sunt nostrud amet',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 55,
              width: 220,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: const Color.fromRGBO(32, 169, 247, 1),
              ),
              child: const Center(
                child: Text(
                  'Rent Now',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontWeight: FontWeight.w400,
                    fontSize: 22,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
